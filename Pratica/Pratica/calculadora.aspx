﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calculadora.aspx.cs" Inherits="Pratica.calculadora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="stylesheet" href="calculadora.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>
        Calculadora
    </title>
</head>
<body>
    <div class="form-container">
        <form id="calculadora" runat="server">
            <h1>CALCULADORA</h1>
            <div class="textBox-container">
                <label for="txtNumero1">Número 1:</label>
                <asp:TextBox ID="txtNumero1" runat="server" CssClass="custom-textBox"></asp:TextBox>
            </div>
            <div class="textBox-container">
                <label for="txtNumero2">Número 2:</label>
                <asp:TextBox ID="txtNumero2" runat="server" CssClass="custom-textBox"></asp:TextBox>
            </div>
            
            <div class="dropDownList-container">
                <label for="selecionarOperacao">Operação:</label>
                <asp:DropDownList ID="selecionarOperacao" runat="server" CssClass="custom-DropDownList">
                    <asp:ListItem Text="Adição" Value="soma"></asp:ListItem>
                    <asp:ListItem Text="Subtração" Value="subtracao"></asp:ListItem>
                    <asp:ListItem Text="Multiplicação" Value="multiplicacao"></asp:ListItem>
                    <asp:ListItem Text="Divisão" Value="divisao"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="button-container">
                <asp:Button ID="btnCalcular" runat="server" Text="Calcular" OnClick="btnCalcular_Click" CssClass="custom-button"/>
            </div>
            <div class="resultado-container">
                <label for="lblResultado"></label>
                <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
            </div>
        </form>
    </div>
</body>
</html>
