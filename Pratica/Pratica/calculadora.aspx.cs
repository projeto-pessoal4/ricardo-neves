﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pratica
{
    public partial class calculadora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnCalcular_Click(object sender, EventArgs e) {
            try {
                double numero1 = Convert.ToDouble(txtNumero1.Text);
                double numero2 = Convert.ToDouble(txtNumero2.Text);
                string operacao = selecionarOperacao.SelectedValue;
                double resultado = 0;

                switch (operacao) {
                    case "soma":
                        resultado = numero1 + numero2;
                        break;
                    case "subtracao":
                        resultado = numero1 - numero2;
                        break;
                    case "multiplicacao":
                        resultado = numero1 * numero2;
                        break;
                    case "divisao":
                        if (numero2 != 0) {
                            resultado = numero1 / numero2;
                        }
                        else {
                            lblResultado.Text = "Erro: Divisão por zero!";
                            return;
                        }
                        break;
                    default:
                        lblResultado.Text = "Operação inválida!";
                        return;
                }

                lblResultado.Text = "O resultado da " + operacao + " foi: " + resultado.ToString();
            
            } catch (Exception ex) {
                lblResultado.Text = "Erro: " + ex.Message;
            }
        }
    }
}