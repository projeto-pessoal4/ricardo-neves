﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="diasDaSemana.aspx.cs" Inherits="Pratica.diasDaSemana" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dias da semana</title>
</head>
<body>
    <form id="diasDaSemana" runat="server">
        <h1>Escolha os dias da semana em que você está disponível para trabalhar:</h1>
        <asp:CheckBoxList ID="diasSemana" runat="server">
            <asp:ListItem Text="Domingo" Value="domingo"></asp:ListItem>
            <asp:ListItem Text="Segunda-feira" Value="segunda"></asp:ListItem>
            <asp:ListItem Text="Terça-feira" Value="terca"></asp:ListItem>
            <asp:ListItem Text="Quarta-feira" Value="quarta"></asp:ListItem>
            <asp:ListItem Text="Quinta-feira" Value="quinta"></asp:ListItem>
            <asp:ListItem Text="Sexta-feira" Value="sexta"></asp:ListItem>
            <asp:ListItem Text="Sábado" Value="sabado"></asp:ListItem>
        </asp:CheckBoxList>
        <br />
        <asp:Button ID="btnMostrarDiasSelecionados" runat="server" Text="Mostrar Dias Selecionados" OnClick="btnMostrarDiasSelecionados_Click" />
        <div>
            <h2>Dias Selecionados:</h2>
            <asp:Label ID="lblDiasSelecionados" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
